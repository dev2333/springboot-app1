package com.test.example1.demo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages ="com.test")
public class Demo1Application {
	public static void main(String[] args) {
		System.out.println("starting app1");
		SpringApplication.run(Demo1Application.class, args);
	}

}
