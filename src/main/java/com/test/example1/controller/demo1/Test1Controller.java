package com.test.example1.controller.demo1;

import com.test.example1.controller.demo1.dto.ResponseObject;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping
public class Test1Controller {
    RestTemplate restTemplate = new RestTemplate();
    @GetMapping("reverse/{incoming}")
    public ResponseObject reverseString (@PathVariable String incoming) {
        ResponseObject responseObject = new ResponseObject();
        responseObject.setIncoming(incoming+":::::*****###");
        responseObject.setOutGoing(StringUtils.hasLength(incoming) ?
                new StringBuilder(incoming).reverse().toString() : "Empty String");
        return responseObject;
    }
	 @GetMapping("reverse")
    public ResponseObject reverseNull () {
        ResponseObject responseObject = new ResponseObject();
        responseObject.setIncoming(".......");
        responseObject.setOutGoing("************");
        return responseObject;
    }
    @GetMapping("url")
    public Object url () {
        Object obj =restTemplate.getForEntity("http://10.2.0.134:3000/url",Object.class);
        return obj;
    }

    @GetMapping
    public ResponseObject reverseEmpty () {
        ResponseObject responseObject = new ResponseObject();
        responseObject.setIncoming("No path variable ");
        responseObject.setOutGoing("Empty String");
        return responseObject;
    }

}
