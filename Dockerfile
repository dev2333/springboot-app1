FROM eclipse-temurin:17.0.3_7-jre
Volume /tmp
ADD /target/*.jar demo1-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/demo1-0.0.1-SNAPSHOT.jar"]
